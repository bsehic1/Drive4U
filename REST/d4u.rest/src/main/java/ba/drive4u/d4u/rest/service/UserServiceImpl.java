package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.User;
import ba.drive4u.d4u.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User getUserById(Integer id) {
        try {
            System.out.println("UserServiceImpl: " + Integer.toString(id));
            User user = userRepository.getOne(id);
            return user;
        } catch (Exception e) {
            System.out.println("Error UserServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteUser(Integer id) {
        try {
            System.out.println("UserServiceImpl: " + Integer.toString(id));
            userRepository.delete(id);

        } catch (Exception e) {
            System.out.println("Error UserServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
