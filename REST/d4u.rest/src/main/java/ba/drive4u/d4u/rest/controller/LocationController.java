package ba.drive4u.d4u.rest.controller;

import ba.drive4u.d4u.rest.model.Location;
import ba.drive4u.d4u.rest.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/locations")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping("/route/{id}")
    public List<Location> getLocationsByRoute(@PathVariable(value = "id") Integer id){
        return locationService.getLocationsByRouteId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Location> getLocation(@PathVariable(value = "id") Integer id){
        Location location = locationService.getLocationById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(location);
    }

    @PostMapping("/")
    public Location createLocation(@Valid @RequestBody Location location){
        return locationService.createLocation(location);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Location> updateLocation(@PathVariable(value = "id") Integer id, @Valid @RequestBody Location location){
        Location oldLocation = locationService.getLocationById(id);
        if(oldLocation == null){
            return ResponseEntity.notFound().build();
        }
        oldLocation = location;
        locationService.updateLocation(oldLocation);
        return ResponseEntity.ok(oldLocation);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable(value = "id") Integer id){
        Location location =locationService.getLocationById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        locationService.deleteLocation(id);
        return ResponseEntity.ok().build();
    }
}
