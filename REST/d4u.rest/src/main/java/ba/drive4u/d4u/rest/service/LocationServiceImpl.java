package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Location;
import ba.drive4u.d4u.rest.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Location updateLocation(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public Location getLocationById(Integer id) {
        try {
            System.out.println("LocationServiceImpl: " + Integer.toString(id));
            Location location = locationRepository.getOne(id);
            return location;
        } catch (Exception e) {
            System.out.println("Error LocationServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteLocation(Integer id) {
        try {
            System.out.println("LocationServiceImpl: " + Integer.toString(id));
            locationRepository.delete(id);

        } catch (Exception e) {
            System.out.println("Error LocationServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Location> getLocationsByRouteId(Integer id) {
        return locationRepository.getLocationsByRouteId(id);
    }

}
