package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.User;

import java.util.List;

public interface UserService {

    User createUser(User user);
    User updateUser(User user);
    User getUserById(Integer id);
    void deleteUser(Integer id);
    List<User> getAllUsers();

}
