package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Route;

import java.util.List;

public interface RouteService {
    Route createRoute(Route route);
    Route updateRoute(Route route);
    Route getRouteById(Integer id);
    void deleteRoute(Integer id);
    List<Route> getRoutesByUserId(Integer id);
}
