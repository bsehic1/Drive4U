package ba.drive4u.d4u.rest.repository;

import ba.drive4u.d4u.rest.model.Location;
import ba.drive4u.d4u.rest.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<Location,Integer> {
    List<Location> getLocationsByRouteId(Integer id);
}
