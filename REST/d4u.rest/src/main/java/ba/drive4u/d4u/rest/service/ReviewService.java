package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Review;

import java.util.List;

public interface ReviewService {

    Review createReview(Review review);
    Review updateReview(Review review);
    Review getReviewById(Integer id);
    void deleteReview(Integer id);
    List<Review> getReviewsByReviewerId(Integer id);
    List<Review> getReviewsByRevieweeId(Integer id);
}
