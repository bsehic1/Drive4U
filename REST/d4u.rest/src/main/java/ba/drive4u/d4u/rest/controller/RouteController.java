package ba.drive4u.d4u.rest.controller;

import ba.drive4u.d4u.rest.model.Route;
import ba.drive4u.d4u.rest.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/routes")
public class RouteController {
    
    @Autowired
    private RouteService routeService;

    @GetMapping("/user/{id}")
    public List<Route> getRoutesByUser(@PathVariable(value = "id") Integer id){
        return routeService.getRoutesByUserId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Route> getRoute(@PathVariable(value = "id") Integer id){
        Route route = routeService.getRouteById(id);
        if(route == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(route);
    }

    @PostMapping("/")
    public Route createRoute(@Valid @RequestBody Route route){
        return routeService.createRoute(route);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Route> updateRoute(@PathVariable(value = "id") Integer id, @Valid @RequestBody Route route){
        Route oldRoute = routeService.getRouteById(id);
        if(oldRoute == null){
            return ResponseEntity.notFound().build();
        }
        oldRoute = route;
        routeService.updateRoute(oldRoute);
        return ResponseEntity.ok(oldRoute);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Route> deleteRoute(@PathVariable(value = "id") Integer id){
        Route route =routeService.getRouteById(id);
        if(route == null){
            return ResponseEntity.notFound().build();
        }
        routeService.deleteRoute(id);
        return ResponseEntity.ok().build();
    }

}
