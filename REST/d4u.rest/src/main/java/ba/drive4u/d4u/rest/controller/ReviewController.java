package ba.drive4u.d4u.rest.controller;

import ba.drive4u.d4u.rest.model.Review;
import ba.drive4u.d4u.rest.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping("/reviewer/{id}")
    public List<Review> getReviewsByReviewer(@PathVariable(value = "id") Integer id){
        return reviewService.getReviewsByReviewerId(id);
    }

    @GetMapping("/reviewee/{id}")
    public List<Review> getReviewsByReviewee(@PathVariable(value = "id") Integer id){
        return reviewService.getReviewsByRevieweeId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Review> getReview(@PathVariable(value = "id") Integer id){
        Review location = reviewService.getReviewById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(location);
    }

    @PostMapping("/")
    public Review createReview(@Valid @RequestBody Review review){
        return reviewService.createReview(review);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Review> updateReview(@PathVariable(value = "id") Integer id, @Valid @RequestBody Review review){
        Review oldReview = reviewService.getReviewById(id);
        if(oldReview == null){
            return ResponseEntity.notFound().build();
        }
        oldReview = review;
        reviewService.updateReview(oldReview);
        return ResponseEntity.ok(oldReview);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Review> deleteReview(@PathVariable(value = "id") Integer id){
        Review review =reviewService.getReviewById(id);
        if(review == null){
            return ResponseEntity.notFound().build();
        }
        reviewService.deleteReview(id);
        return ResponseEntity.ok().build();
    }
}
