package ba.drive4u.d4u.rest.repository;

import ba.drive4u.d4u.rest.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteRepository extends JpaRepository<Route,Integer>{
    List<Route> getRoutesByCreatedById(Integer id);
}
