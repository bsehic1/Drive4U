package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Location;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocationService {
    Location createLocation(Location location);
    Location updateLocation(Location location);
    Location getLocationById(Integer id);
    void deleteLocation(Integer id);
    List<Location> getLocationsByRouteId(Integer id);
}
