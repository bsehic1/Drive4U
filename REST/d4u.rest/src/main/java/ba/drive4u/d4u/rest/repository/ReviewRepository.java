package ba.drive4u.d4u.rest.repository;

import ba.drive4u.d4u.rest.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review,Integer>{
    List<Review> getReviewsByRevieweeId(Integer id);
    List<Review> getReviewsByReviewerId(Integer id);
}
