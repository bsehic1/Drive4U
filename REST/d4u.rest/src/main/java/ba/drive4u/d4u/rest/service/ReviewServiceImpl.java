package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Review;
import ba.drive4u.d4u.rest.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public Review createReview(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public Review updateReview(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public Review getReviewById(Integer id) {
        try {
            System.out.println("ReviewServiceImpl: " + Integer.toString(id));
            Review review = reviewRepository.getOne(id);
            return review;
        } catch (Exception e) {
            System.out.println("Error ReviewServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteReview(Integer id) {
        try {
            System.out.println("ReviewServiceImpl: " + Integer.toString(id));
            reviewRepository.delete(id);

        } catch (Exception e) {
            System.out.println("Error ReviewServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Review> getReviewsByReviewerId(Integer id) {
        return reviewRepository.getReviewsByReviewerId(id);
    }

    @Override
    public List<Review> getReviewsByRevieweeId(Integer id) {
        return reviewRepository.getReviewsByRevieweeId(id);
    }
}
