package ba.drive4u.d4u.rest.service;

import ba.drive4u.d4u.rest.model.Route;
import ba.drive4u.d4u.rest.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public Route createRoute(Route route) {
        return routeRepository.save(route);
    }

    @Override
    public Route updateRoute(Route route) {
        return routeRepository.save(route);
    }

    @Override
    public Route getRouteById(Integer id) {
        try {
            System.out.println("RouteServiceImpl: " + Integer.toString(id));
            Route route = routeRepository.getOne(id);
            return route;
        } catch (Exception e) {
            System.out.println("Error RouteServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteRoute(Integer id) {
        try {
            System.out.println("RouteServiceImpl: " + Integer.toString(id));
            routeRepository.delete(id);

        } catch (Exception e) {
            System.out.println("Error RouteServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Route> getRoutesByUserId(Integer id) {
        return routeRepository.getRoutesByCreatedById(id);
    }
}
