package com.d4u.drive4u.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    SweetAlertDialog pDialog;

    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleSignInClient;

    private static final int RC_SIGN_IN = 1;
    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        Button mLogInButton = findViewById(R.id.login_log_in_button);
        Button mRegisterButton = findViewById(R.id.login_register_button);
        mUsernameEditText = findViewById(R.id.login_username_edit_text);
        mPasswordEditText = findViewById(R.id.login_password_edit_text);
        ImageButton mGoogleSignInImageButton = findViewById(R.id.login_google_sign_in_button);

        mRegisterButton.setOnClickListener(view -> {
            goToRegister();
        });

        mLogInButton.setOnClickListener(view -> {
            logUserIn();
        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, connectionResult -> Toast.makeText(LoginActivity.this,"Connecting to Google Sign In failed!",Toast.LENGTH_SHORT).show())
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        mGoogleSignInImageButton.setOnClickListener(v -> {
            signIn();
        });
        //
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null){
            goToMain();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if(result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } else {
                SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
                dialog.setContentText("Can't authenticate user.");
                dialog.show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE);
                        dialog.setContentText("Succesfully Authenticated!");
                        dialog.show();
                        Log.d(TAG, "signInWithCredential:success");
                        goToMain();

                    } else {
                        SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
                        dialog.setContentText(Objects.requireNonNull(task.getException()).getMessage());
                        dialog.show();
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        goToLogin();
                    }
                });
    }

    private void goToLogin() {
        Intent intent = new Intent(LoginActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void logUserIn() {
        String email = mUsernameEditText.getText().toString();
        String password = mPasswordEditText.getText().toString();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Please enter username",Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Please enter password",Toast.LENGTH_SHORT).show();
        } else{
            mAuth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            goToMain();
                        } else{
                            SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
                            dialog.setContentTextSize(10);
                            dialog.setContentText(Objects.requireNonNull(task.getException()).getMessage());
                            dialog.show();
                        }
                    });
        }
    }

    private void goToRegister() {
        Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(intent);
    }

    private void goToMain(){
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
