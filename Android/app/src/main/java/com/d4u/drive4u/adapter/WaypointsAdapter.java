package com.d4u.drive4u.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.dto.Location;

import java.text.DateFormat;
import java.util.List;

public class WaypointsAdapter extends ArrayAdapter<Location> {
    private Context _context;
    private List<Location> locationList;

    public WaypointsAdapter(@NonNull Context context, List<Location> locations){
        super(context, 0, locations);
        _context = context;
        locationList = locations;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(_context).inflate(R.layout.waypoint_list_item,parent,false);

        TextView title = listItem.findViewById(R.id.waypoint_list_item_title_text_view);
        TextView time = listItem.findViewById(R.id.waypoint_list_item_time_text_view);

        title.setText(locationList.get(position).getTitle());
        time.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(locationList.get(position).getTime()));

        return listItem;
    }

    @Override
    public int getCount() {
        if (locationList == null) {
            return 0;
        } else {
            return locationList.size();
        }
    }
}
