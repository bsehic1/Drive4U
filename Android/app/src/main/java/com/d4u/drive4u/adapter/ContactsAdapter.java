package com.d4u.drive4u.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.activity.MainActivity;
import com.d4u.drive4u.domainModel.UserModel;
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private Context context;
    private List<UserModel> mDataset;

    @NonNull
    @Override
    public String getSectionName(int position) {
        return mDataset.get(position).getUsername().substring(0,1).toUpperCase();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder{


        private TextView mUsernameTextView;
        private TextView mFullNameTextView;
        private CircleImageView mImageView;
        private RelativeLayout mRelativeLayout;


        ContactViewHolder(View v){
            super(v);
            mRelativeLayout = v.findViewById(R.id.contacts_list_item_relative_layout);
            mUsernameTextView = v.findViewById(R.id.contacts_list_item_username_text_view);
            mFullNameTextView = v.findViewById(R.id.contacts_list_item_full_name_text_view);
            mImageView = v.findViewById(R.id.contacts_list_item_image_view);
        }
    }

    //TODO:"Sorting contacts that are currently strings"
    public ContactsAdapter(Context context, List<UserModel> contactList){
        this.context = context;
        Collections.sort(contactList,((o1, o2) -> o1.getUsername().compareTo(o2.getUsername())));
        mDataset = contactList;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contacts_list_item, parent, false);

        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {

        holder.mUsernameTextView.setText(mDataset.get(position).getUsername());
        holder.mFullNameTextView.setText(mDataset.get(position).getFullName());
        Picasso.get().load(mDataset.get(position).getProfileImageUrl()).placeholder(R.drawable.profile_image_placeholder).into(holder.mImageView);
        holder.mRelativeLayout.setOnClickListener(v -> {
            ((MainActivity)context).goToProfileFragment(mDataset.get(position).getUserId());
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
