package com.d4u.drive4u.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.activity.MainActivity;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.service.RouteService;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.maps.android.PolyUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreateRouteOriginAndDestinationFragment extends Fragment implements OnMapReadyCallback{

    private MapView mMapView;
    private GoogleMap mGoogleMap;

    private Button mCancelButton;
    private Button mNextStepButton;
    private EditText mOriginEditText;
    private EditText mOriginTimeEditText;
    private EditText mDestinationEditText;
    private EditText mDestinationTimeEditText;
    SweetAlertDialog pDialog;

    private RouteService routeService;

    private Location origin;
    private Location destination;

    private Marker originMarker;
    private Marker destinationMarker;

    private List<Polyline> polylineList;

    private byte markerCounter = 0;

    private Route route;

    private int PERMISSION_CODE = 1;

    private DateFormat shortTimeFormat;

    public CreateRouteOriginAndDestinationFragment() {
        polylineList = new ArrayList<>();
        routeService = new RouteService();
        shortTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT);
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_origin_and_destination, container, false);

        pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorTurquoise2));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        mMapView = v.findViewById(R.id.origin_and_destination_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        mCancelButton = v.findViewById(R.id.origin_and_destination_cancel_button);
        mNextStepButton = v.findViewById(R.id.origin_and_destination_next_step_button);
        mOriginEditText = v.findViewById(R.id.origin_and_destination_origin_edit_text);
        mOriginTimeEditText = v.findViewById(R.id.origin_and_destination_origin_time_edit_text);
        mDestinationEditText = v.findViewById(R.id.origin_and_destination_destination_edit_text);
        mDestinationTimeEditText = v.findViewById(R.id.origin_and_destination_destination_time_edit_text);

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        if(ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions();
        }

        googleMap.setMyLocationEnabled(true);
        CameraUpdate cameraUpdate;
        if(route != null){
            markerCounter = 2;
            mNextStepButton.setEnabled(true);
            mNextStepButton.setAlpha(1);
            origin = route.getOrigin();
            destination = route.getDestination();
            originMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(origin.getLatitude(),origin.getLongitude())));
            destinationMarker = mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(destination.getLatitude(),destination.getLongitude())));
            drawPolylineByLocations();
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(origin.getLatitude(),origin.getLongitude()), 15);
            if(origin != null){
                if(origin.getTitle() != null){
                    mOriginEditText.setText(origin.getTitle());
                }
                if(origin.getTime() != null){
                    String time = shortTimeFormat.format(origin.getTime());
                    mOriginTimeEditText.setText(time);
                }
            }
            if(destination != null){
                if(destination.getTitle() != null){
                    mDestinationEditText.setText(destination.getTitle());
                }
                if(destination.getTime() != null){
                    String time = shortTimeFormat.format(destination.getTime());
                    mDestinationTimeEditText.setText(time);
                }
            }
        } else {
            route = new Route();
            origin = new Location();
            destination = new Location();
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.857498, 18.421586), 15);
        }

        pDialog.dismiss();

        googleMap.animateCamera(cameraUpdate);

        mGoogleMap.setOnMapClickListener(latLng -> {
            pDialog.show();
            if(markerCounter == 0){
                originMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                origin = new Location(latLng);
                markerCounter++;
            } else if(markerCounter == 1){
                destinationMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                destination = new Location(latLng);
                markerCounter++;
                drawPolylineByLocations();
                mNextStepButton.setAlpha(1);
                mNextStepButton.setEnabled(true);
            } else if(markerCounter > 1){
                destination = new Location(latLng);
                destinationMarker.remove();
                destinationMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                drawPolylineByLocations();
                mNextStepButton.setAlpha(1);
                mNextStepButton.setEnabled(true);
            }
            pDialog.dismiss();
        });

        mGoogleMap.setOnMarkerClickListener(marker -> {
            pDialog.show();
            if(marker.getPosition().equals(originMarker.getPosition())){
                mGoogleMap.clear();
                markerCounter = 0;
            } else{
                for(Polyline polyline : polylineList){
                    polyline.remove();
                }
                polylineList.clear();
                destinationMarker.remove();
                markerCounter--;
            }
            mNextStepButton.setAlpha((float) 0.5);
            mNextStepButton.setEnabled(false);
            pDialog.dismiss();
            return false;
        });

        mOriginTimeEditText.setOnClickListener(v -> {
            Calendar mCalender = Calendar.getInstance();
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), (view, hourOfDay, minute) -> {
                mCalender.set(Calendar.HOUR_OF_DAY,hourOfDay);
                mCalender.set(Calendar.MINUTE,minute);
                origin.setTime(mCalender.getTime());
                String time = shortTimeFormat.format(mCalender.getTime());
                mOriginTimeEditText.setText(time);
            },0,0,true);
            mTimePicker.setTitle("Set Departure Time");
            mTimePicker.show();
        });

        mDestinationTimeEditText.setOnClickListener(v -> {
            Calendar mCalendar = Calendar.getInstance();
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), (view, hourOfDay, minute) -> {
                mCalendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                mCalendar.set(Calendar.MINUTE,minute);
                destination.setTime(mCalendar.getTime());
                String time = shortTimeFormat.format(mCalendar.getTime());
                mDestinationTimeEditText.setText(time);
            },0,0,true);
            mTimePicker.setTitle("Set Departure Time");
            mTimePicker.show();
        });

        mCancelButton.setOnClickListener(v -> {
            goToProfileFragment();
        });

        mNextStepButton.setOnClickListener(v -> {
            if(mOriginEditText.getText().toString().isEmpty()){
                SweetAlertDialog dialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.ERROR_TYPE);
                dialog.setTitleText("Origin is blank!");
                dialog.show();
            } else if(mDestinationEditText.getText().toString().isEmpty()){
                SweetAlertDialog dialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.ERROR_TYPE);
                dialog.setTitleText("Destination is blank!");
                dialog.show();
            } else {
                origin.setTitle(mOriginEditText.getText().toString());
                destination.setTitle(mDestinationEditText.getText().toString());
                goToWaypointsFragment();
            }
        });
    }

    private void drawPolylineByLocations() {
        routeService.getDirectionsData(origin,destination,null).observe(this, waypoints->{
            if(waypoints!=null){
                for(Polyline polyline: polylineList){
                    polyline.remove();
                }
                polylineList.clear();
                for (String line : waypoints) {
                    Log.w("WAYPOINT:",line);
                    PolylineOptions polylineOptions = new PolylineOptions()
                            .color(Color.BLUE)
                            .width(20)
                            .addAll(PolyUtil.decode(line));
                    polylineList.add(mGoogleMap.addPolyline(polylineOptions));
                }
            }
        });
    }

    private void goToWaypointsFragment(){
        route.setOrigin(origin);
        route.setDestination(destination);
        ((MainActivity) Objects.requireNonNull(getActivity())).goToWaypointsFragment(route,null);
    }

    private void goToProfileFragment(){
        ((MainActivity) Objects.requireNonNull(getActivity())).goToProfileFragment(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
    }

    //region Map Fragment lifecycle methods
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    //endregion

    //region Permission Check Methods
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", (dialog, which) -> ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                            new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permissions GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permissions DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //endregion

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
