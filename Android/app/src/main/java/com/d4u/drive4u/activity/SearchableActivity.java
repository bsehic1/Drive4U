package com.d4u.drive4u.activity;

import android.arch.paging.PagedList;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.domainModel.UserModel;
import com.d4u.drive4u.dto.User;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class SearchableActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TabLayout mTabLayout;
    SearchView mSearchView;

    private CollectionReference usersRef;

    FirestorePagingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        mSearchView = findViewById(R.id.search_view);
        ImageButton mBackButton = findViewById(R.id.back_button);
        mTabLayout = findViewById(R.id.searchable_tab_layout);
        mRecyclerView = findViewById(R.id.search_recycler_view);

        usersRef = FirebaseFirestore.getInstance().collection("users");

        mSearchView.onActionViewExpanded();

        mBackButton.setOnClickListener(view -> {
            onBackPressed();
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                firebaseSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                firebaseSearch(newText);
                return false;
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        if(adapter != null){
            adapter.stopListening();
        }
    }

    void firebaseSearch(String queryText){

        if(mTabLayout.getSelectedTabPosition() == 0){
            Query query = usersRef.orderBy("username").startAt(queryText).endAt(queryText + "\uf8ff");

            PagedList.Config config = new PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPrefetchDistance(10)
                    .setPageSize(20)
                    .build();

            FirestorePagingOptions<User> options = new FirestorePagingOptions.Builder<User>()
                    .setLifecycleOwner(this)
                    .setQuery(query, config, snapshot -> {

                        User user = snapshot.toObject(User.class);
                        Objects.requireNonNull(user).setUserId(snapshot.getId());

                        return user;
                    })
                    .build();

            adapter = new FirestorePagingAdapter<User, UsersViewHolder>(options) {
                @Override
                protected void onBindViewHolder(@NonNull UsersViewHolder holder, int position, @NonNull User model) {
                    holder.mUsernameTextView.setText(model.getUsername());
                    holder.mFullNameTextView.setText(model.getFullName());
                    Picasso.get().load(model.getProfileImageUrl()).placeholder(R.drawable.profile_image_placeholder).into(holder.mImageView);
                    holder.mRelativeLayout.setOnClickListener(v -> {
                        Intent intent = new Intent(SearchableActivity.this,MainActivity.class);
                        intent.putExtra("profile",model.getUserId());
                        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivityIfNeeded(intent,0);
                        finish();
                    });
                }

                @NonNull
                @Override
                public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View itemView = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.contacts_list_item,parent,false);
                    return new UsersViewHolder(itemView);
                }
            };

            adapter.startListening();
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
        } else if(mTabLayout.getSelectedTabPosition() == 1){
            Query query = usersRef.orderBy("username").startAt(queryText).endAt(queryText + "\uf8ff");

            PagedList.Config config = new PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPrefetchDistance(10)
                    .setPageSize(20)
                    .build();

            FirestorePagingOptions<User> options = new FirestorePagingOptions.Builder<User>()
                    .setLifecycleOwner(this)
                    .setQuery(query, config, snapshot -> {

                        User user = snapshot.toObject(User.class);
                        Objects.requireNonNull(user).setUserId(snapshot.getId());

                        return user;
                    })
                    .build();

            adapter = new FirestorePagingAdapter<User, UsersViewHolder>(options) {
                @Override
                protected void onBindViewHolder(@NonNull UsersViewHolder holder, int position, @NonNull User model) {
                    holder.mUsernameTextView.setText(model.getUsername());
                    holder.mFullNameTextView.setText(model.getFullName());
                    Picasso.get().load(model.getProfileImageUrl()).placeholder(R.drawable.profile_image_placeholder).into(holder.mImageView);
                    holder.mRelativeLayout.setOnClickListener(v -> {
                        Intent intent = new Intent(SearchableActivity.this,MainActivity.class);
                        intent.putExtra("profile",model.getUserId());
                        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivityIfNeeded(intent,0);
                        finish();
                    });
                }

                @NonNull
                @Override
                public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View itemView = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.contacts_list_item,parent,false);
                    return new UsersViewHolder(itemView);
                }
            };

            adapter.startListening();
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
        } else if(mTabLayout.getSelectedTabPosition() == 2){
            Query query = usersRef.orderBy("username").startAt(queryText).endAt(queryText + "\uf8ff");

            PagedList.Config config = new PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setPrefetchDistance(10)
                    .setPageSize(20)
                    .build();

            FirestorePagingOptions<User> options = new FirestorePagingOptions.Builder<User>()
                    .setLifecycleOwner(this)
                    .setQuery(query, config, snapshot -> {

                        User user = snapshot.toObject(User.class);
                        Objects.requireNonNull(user).setUserId(snapshot.getId());

                        return user;
                    })
                    .build();

            adapter = new FirestorePagingAdapter<User, UsersViewHolder>(options) {
                @Override
                protected void onBindViewHolder(@NonNull UsersViewHolder holder, int position, @NonNull User model) {
                    holder.mUsernameTextView.setText(model.getUsername());
                    holder.mFullNameTextView.setText(model.getFullName());
                    Picasso.get().load(model.getProfileImageUrl()).placeholder(R.drawable.profile_image_placeholder).into(holder.mImageView);
                    holder.mRelativeLayout.setOnClickListener(v -> {
                        Intent intent = new Intent(SearchableActivity.this,MainActivity.class);
                        intent.putExtra("profile",model.getUserId());
                        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivityIfNeeded(intent,0);
                        finish();
                    });
                }

                @NonNull
                @Override
                public UsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View itemView = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.contacts_list_item,parent,false);
                    return new UsersViewHolder(itemView);
                }
            };

            adapter.startListening();
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setHasFixedSize(true);
        }
    }

    //View Holder
    public class UsersViewHolder extends RecyclerView.ViewHolder{
        TextView mUsernameTextView;
        TextView mFullNameTextView;
        CircleImageView mImageView;
        RelativeLayout mRelativeLayout;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mUsernameTextView = itemView.findViewById(R.id.contacts_list_item_username_text_view);
            mFullNameTextView = itemView.findViewById(R.id.contacts_list_item_full_name_text_view);
            mImageView = itemView.findViewById(R.id.contacts_list_item_image_view);
            mRelativeLayout = itemView.findViewById(R.id.contacts_list_item_relative_layout);
        }

    }

}
