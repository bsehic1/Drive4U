package com.d4u.drive4u.dto;

import com.d4u.drive4u.domainModel.UserModel;

import java.util.Date;
import java.util.List;

public class User {

    private String userId;

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String username;

    private String profileImageUrl;

    private Date registrationDate;

    private Boolean accountValidated;

    private List<Conversation> conversations;

    private List<String> contacts;

    private List<Route> routes;

    public User(){

    }


    //region Getters and Setters
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Boolean getAccountValidated() {
        return accountValidated;
    }

    public void setAccountValidated(Boolean accountValidated) {
        this.accountValidated = accountValidated;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }

    //endregion

    public UserModel toUserModel(){
        UserModel userModel = new UserModel();
        userModel.setFirstName(firstName);
        userModel.setLastName(lastName);
        userModel.setEmail(email);
        userModel.setPhoneNumber(phoneNumber);
        userModel.setUsername(username);
        userModel.setProfileImageUrl(profileImageUrl);
        userModel.setRegistrationDate(registrationDate);
        userModel.setAccountValidated(accountValidated);
        userModel.setContacts(contacts);

        return userModel;
    }
}
