package com.d4u.drive4u.domainModel;

import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class RouteModel {

    private String routeId;

    private String title;

    private String createdById;

    private Date createdDate;

    private String description;

    private Location origin;

    private Location destination;

    private List<Location> locations;

    public RouteModel(){

    }

    //region Getters and Setters
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getOrigin() {
        return origin;
    }

    public void setOrigin(Location origin) {
        this.origin = origin;
    }

    public Location getDestination() {
        return destination;
    }

    public void setDestination(Location destination) {
        this.destination = destination;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    //endregion

    public Route toRoute(){
        Route route = new Route();
        route.setTitle(title);
        route.setDescription(description);
        route.setCreatedDate(createdDate);
        route.setCreatedById(createdById);
        route.setOrigin(origin);
        route.setDestination(destination);
        route.setLocations(getLocations());
        return route;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }
}