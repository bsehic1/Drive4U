package com.d4u.drive4u.dto;

import com.d4u.drive4u.domainModel.RouteModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Route {

    private String routeId;

    private String title;

    private String createdById;

    private Date createdDate;

    private String description;

    private Location origin;

    private Location destination;

    private List<Location> locations;

    public Route(){

    }

    //region Getters and Setters
    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getOrigin() {
        return origin;
    }

    public void setOrigin(Location origin) {
        this.origin = origin;
    }

    public Location getDestination() {
        return destination;
    }

    public void setDestination(Location destination) {
        this.destination = destination;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    //endregion

    public RouteModel toRouteModel(){
        RouteModel routeModel = new RouteModel();
        routeModel.setTitle(title);
        routeModel.setDescription(description);
        routeModel.setCreatedDate(createdDate);
        routeModel.setCreatedById(createdById);
        routeModel.setOrigin(origin);
        routeModel.setDestination(destination);
        routeModel.setLocations(getLocations());
        return routeModel;
    }
}
