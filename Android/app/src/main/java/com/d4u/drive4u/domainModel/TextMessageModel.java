package com.d4u.drive4u.domainModel;

import java.util.Date;

import javax.annotation.Nullable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TextMessageModel {

    private String conversationId;

    private String senderId;

    private String messageText;

    private Date sentTimeStamp;

    @Nullable
    private Date receivedTimestamp;

    public TextMessageModel(){

    }

    //region Getters and Setters
    public Date getSentTimeStamp() {
        return sentTimeStamp;
    }

    public void setSentTimeStamp(Date sentTimeStamp) {
        this.sentTimeStamp = sentTimeStamp;
    }

    @Nullable
    public Date getReceivedTimestamp() {
        return receivedTimestamp;
    }

    public void setReceivedTimestamp(@Nullable Date receivedTimestamp) {
        this.receivedTimestamp = receivedTimestamp;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    //endregion
}
