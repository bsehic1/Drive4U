package com.d4u.drive4u.repository;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DirectionsRepository {

    @GET("json?")
    Single<String> getDirections(@Query("origin") String origin, @Query("destination") String destination, @Query("waypoints") String waypoints, @Query("key")String key);

}
