package com.d4u.drive4u.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.utils.UIUtils;
import com.d4u.drive4u.activity.MainActivity;
import com.d4u.drive4u.adapter.WaypointsAdapter;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreateRouteDetailsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private Route route;
    private List<Location> locationList;

    private WaypointsAdapter waypointsAdapter;

    private EditText mRouteTitleEditText;
    private EditText mRouteDateEditText;
    private EditText mRoutePlacesEidtText;
    private EditText mRouteDescriptionEditText;
    private TextView mOriginTextView;
    private TextView mDestinationTextView;
    private ListView mLocationsListView;
    private SweetAlertDialog pDialog;

    private Button mSaveButton;
    private Button mPreviousStepButton;

    public CreateRouteDetailsFragment() {
        locationList = new ArrayList<>();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_route_details, container, false);

        //region Getting views by id
        mRouteTitleEditText = v.findViewById(R.id.create_route_title_edit_text);
        mRouteDescriptionEditText = v.findViewById(R.id.create_route_description_edit_text);
        mOriginTextView = v.findViewById(R.id.create_route_details_origin_text_text_view);
        mDestinationTextView = v.findViewById(R.id.create_route_details_destination_text_text_view);
        mLocationsListView = v.findViewById(R.id.create_route_details_list_view);
        mSaveButton = v.findViewById(R.id.create_route_details_save_button);
        mPreviousStepButton = v.findViewById(R.id.create_route_details_previous_step_button);
        //endregion

        pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorTurquoise2));

        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        mOriginTextView.setText(route.getOrigin().getTitle());
        mDestinationTextView.setText(route.getDestination().getTitle());

        waypointsAdapter = new WaypointsAdapter(Objects.requireNonNull(this.getActivity()), locationList);
        mLocationsListView.setAdapter(waypointsAdapter);
        UIUtils.setListViewHeightBasedOnItems(mLocationsListView);

        pDialog.dismiss();

        mSaveButton.setOnClickListener(view -> {
            route.setTitle(mRouteTitleEditText.getText().toString());
            // Broj mijesta
            // Datum
            // Opis itd

            // SNIMITI RUTU NA WEB
        });

        mPreviousStepButton.setOnClickListener(view->{
            goToWaypointsFragment();
        });

        return v;
    }

    private void goToWaypointsFragment() {
        ((MainActivity) Objects.requireNonNull(getActivity())).goToWaypointsFragment(route, locationList);
    }


    public List<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    //region Fragment factory methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //endregion
}
