package com.d4u.drive4u.dal;

import io.realm.RealmConfiguration;

/**
 * Created by benjamin.sehic on 02.03.2018.
 */

public class D4URealmConfiguration {

    public static RealmConfiguration config = null;

    public static RealmConfiguration getRealmConfiguration(){
        if(config == null){
            config = new RealmConfiguration.Builder()
                    .name("d4urealm.realm")
                    .build();
        }
        return config;
    }
}
