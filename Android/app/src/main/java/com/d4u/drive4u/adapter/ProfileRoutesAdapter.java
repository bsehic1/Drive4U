package com.d4u.drive4u.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.dto.Route;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by benjamin.sehic on 14.03.2018.
 */

public class ProfileRoutesAdapter extends ArrayAdapter<Route> {

    private Context _context;
    private List<Route> routeList;

    public ProfileRoutesAdapter(@NonNull Context context, List<Route> routes){
        super(context, 0, routes);
        _context = context;
        routeList = routes;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(_context).inflate(R.layout.routes_list_item,parent,false);

        Route currentRoute = routeList.get(position);


        TextView mTitleTextView = listItem.findViewById(R.id.routes_list_item_title_text_view);
        TextView mStartTimeTextView = listItem.findViewById(R.id.routes_list_item_start_time_text_view);
        TextView mEndTimeTextView = listItem.findViewById(R.id.routes_list_item_end_time_text_view);

        mTitleTextView.setText(currentRoute.getTitle());
        mStartTimeTextView.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(currentRoute.getOrigin().getTime()));
        mEndTimeTextView.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(currentRoute.getDestination().getTime()));

        return listItem;
    }

    @Override
    public int getCount() {
        if (routeList == null) {
            return 0;
        } else {
            return routeList.size();
        }
    }
}
