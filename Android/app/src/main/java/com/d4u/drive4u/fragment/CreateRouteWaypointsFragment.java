package com.d4u.drive4u.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.utils.UIUtils;
import com.d4u.drive4u.activity.MainActivity;
import com.d4u.drive4u.adapter.CreateWaypointsAdapter;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.service.RouteService;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreateRouteWaypointsFragment extends Fragment implements OnMapReadyCallback {

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private ListView mWaypointsListView;
    private SweetAlertDialog pDialog;

    private CreateWaypointsAdapter createWaypointsAdapter;

    private List<Location> locationList;
    private List<Marker> markerList;
    private List<Polyline> polylineList;

    private Marker originMarker;
    private Marker destinationMarker;

    private Route route;
    private RouteService routeService;

    private int PERMISSION_CODE = 1;

    public CreateRouteWaypointsFragment() {
        routeService = new RouteService();
        markerList = new ArrayList<>();
        locationList = new ArrayList<>();
        polylineList = new ArrayList<>();
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_waypoints, container, false);

        pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();


        mMapView = v.findViewById(R.id.waypoints_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        mWaypointsListView = v.findViewById(R.id.waypoints_list_view);
        createWaypointsAdapter = new CreateWaypointsAdapter(getActivity(), locationList);
        mWaypointsListView.setAdapter(createWaypointsAdapter);
        UIUtils.setListViewHeightBasedOnItems(mWaypointsListView);

        Button mPreviousStepButton = v.findViewById(R.id.waypoints_previous_step_button);
        Button mNextStepButton = v.findViewById(R.id.waypoints_next_step_button);

        mPreviousStepButton.setOnClickListener(v1 -> {
            goToOriginAndDestinationFragment();
        });

        mNextStepButton.setOnClickListener(v1 -> {
            for(int i = 0; i < mWaypointsListView.getCount();i++){
                View view = mWaypointsListView.getChildAt(i);
                EditText editText = view.findViewById(R.id.waypoint_list_item_title_text_view);
                locationList.get(i).setTitle(editText.getText().toString());
                //DODATI I VREMENA NA OVOJ LINIJI
            }
            goToCreateRouteDetailsFragment();
        });

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        if(ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions();
        }

        googleMap.setMyLocationEnabled(true);

        originMarker = mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(route.getOrigin().getLatitude(), route.getOrigin().getLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        destinationMarker = mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(route.getDestination().getLatitude(), route.getDestination().getLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

       for(Location location : locationList){
           markerList.add(mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))));
       }

        drawPolylineByLocations(mGoogleMap, route.getOrigin(), route.getDestination(), locationList);

       pDialog.dismiss();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(originMarker.getPosition(), 15);
        googleMap.animateCamera(cameraUpdate);

        mGoogleMap.setOnMapClickListener(latLng -> {
            pDialog.show();
            locationList.add(new Location(latLng));
            //TODO:Add custom Bitmap for markers to know which is which
            markerList.add(mGoogleMap.addMarker(new MarkerOptions().position(latLng)));
            drawPolylineByLocations(mGoogleMap, route.getOrigin(), route.getDestination(), locationList);
            createWaypointsAdapter.notifyDataSetChanged();
            UIUtils.setListViewHeightBasedOnItems(mWaypointsListView);
            pDialog.dismiss();
        });

        mGoogleMap.setOnMarkerClickListener(marker -> {
            pDialog.show();
            if(!(marker.getPosition().latitude == route.getOrigin().getLatitude() && marker.getPosition().longitude == route.getOrigin().getLongitude()) &&
                    !(marker.getPosition().latitude == route.getDestination().getLatitude() && marker.getPosition().longitude == route.getDestination().getLongitude())){
                for(int i = 0; i < markerList.size();i++){
                    if(markerList.get(i).getPosition().equals(marker.getPosition())){
                        marker.remove();
                        markerList.remove(i);
                        locationList.remove(i);
                        drawPolylineByLocations(mGoogleMap, route.getOrigin(), route.getDestination(), locationList);
                        createWaypointsAdapter.notifyDataSetChanged();
                        UIUtils.setListViewHeightBasedOnItems(mWaypointsListView);
                        break;
                    }
                }
            }
            pDialog.dismiss();
            return false;
        });
    }



    private void drawPolylineByLocations(GoogleMap googleMap, Location origin, Location destination, List<Location> locationList) {
        routeService.getDirectionsData(origin,destination,locationList).observe(this, waypoints->{
            if (waypoints != null && waypoints.size() > 0) {
                for(Polyline polyline : polylineList){
                    polyline.remove();
                }
                polylineList.clear();
                for (String line : waypoints) {
                    PolylineOptions polylineOptions = new PolylineOptions()
                            .color(Color.BLUE)
                            .width(15)
                            .addAll(PolyUtil.decode(line));
                    polylineList.add(googleMap.addPolyline(polylineOptions));
                }
            } else {
                Toast.makeText(getActivity(),"Can't route through given waypoint!",Toast.LENGTH_LONG).show();
            }
        });
    }

    //region Map Fragment lifecycle methods
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    //endregion

    //region Permission Check Methods
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", (dialog, which) -> ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                            new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permissions GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permissions DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //endregion


    public void goToOriginAndDestinationFragment(){
        ((MainActivity) Objects.requireNonNull(getActivity())).goToOriginAndDestinationFragment(route);
    }

    public void goToCreateRouteDetailsFragment(){
        ((MainActivity) Objects.requireNonNull(getActivity())).goToCreateRouteDetailsFragment(route, locationList);
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
