package com.d4u.drive4u.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.domainModel.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Date;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class SetupActivity extends AppCompatActivity {

    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mUsernameEditText;
    private EditText mPhoneNumberEditText;
    private CircleImageView mUserImageView;
    private SweetAlertDialog pDialog;

    private DocumentReference userRef;
    private StorageReference userProfileImageRef;

    FirebaseAuth mAuth;

    private String currentUserId;

    final static int gallery_pick = 1;

    private Uri resultUri;
    private String downloadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        mFirstNameEditText = findViewById(R.id.setup_first_name_edit_text);
        mLastNameEditText = findViewById(R.id.setup_last_name_edit_text);
        mUsernameEditText = findViewById(R.id.setup_username_edit_text);
        mPhoneNumberEditText = findViewById(R.id.setup_phone_number_edit_text);
        mUserImageView = findViewById(R.id.setup_user_image_view);
        Button mCreateAccountButton = findViewById(R.id.setup_create_account_button);

        mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        currentUserId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        userRef = db.collection("users").document(currentUserId);
        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");

        fillSetupInfoIfExists();


        mCreateAccountButton.setOnClickListener(view -> {
            saveAccountInformation();
        });

        mUserImageView.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, gallery_pick);
        });
    }

    private void fillSetupInfoIfExists() {
        if(getIntent().getExtras() != null) {
            resultUri = (Uri) getIntent().getExtras().get("profileImageUrl");
            if (resultUri != null) {
                mUserImageView.setImageURI(resultUri);
            }
            if(getIntent().getExtras().containsKey("profileImageDownloadUrl")){
                downloadUrl = getIntent().getExtras().getString("profileImageDownloadUrl");
            }
            if (getIntent().getExtras().containsKey("firstNameValue")) {
                mFirstNameEditText.setText(getIntent().getExtras().getString("firstNameValue"));
            }
            if (getIntent().getExtras().containsKey("lastNameValue")) {
                mLastNameEditText.setText(getIntent().getExtras().getString("lastNameValue"));
            }
            if (getIntent().getExtras().containsKey("usernameValue")) {
                mUsernameEditText.setText(getIntent().getStringExtra("usernameValue"));
            }
            if (getIntent().getExtras().containsKey("phoneNumberValue")) {
                mPhoneNumberEditText.setText(getIntent().getStringExtra("phoneNumberValue"));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



       if(requestCode == gallery_pick && resultCode == RESULT_OK && data != null){
           Uri imageUri = data.getData();

           CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true);
            pDialog.show();

            if(resultCode == RESULT_OK){
                resultUri = result.getUri();
                StorageReference filePath = userProfileImageRef.child(currentUserId + ".jpg");
                filePath.putFile(resultUri).addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        Toast.makeText(this, "Profile image stored to storage successfully!", Toast.LENGTH_SHORT).show();

                        filePath.getDownloadUrl()
                                .addOnSuccessListener(uri -> {
                            downloadUrl = uri.toString();
                            userRef.update("profileImageUrl",downloadUrl)
                                    .addOnCompleteListener(task1 -> {
                                        if(task.isSuccessful()){

                                            Intent intent = new Intent(SetupActivity.this, SetupActivity.class);
                                            intent.putExtra("profileImageUrl", resultUri);
                                            intent.putExtra("profileImageDownloadUrl",downloadUrl);
                                            intent.putExtra("firstNameValue", mFirstNameEditText.getText().toString());
                                            intent.putExtra("lastNameValue", mLastNameEditText.getText().toString());
                                            intent.putExtra("usernameValue", mUsernameEditText.getText().toString());
                                            intent.putExtra("phoneNumberValue", mPhoneNumberEditText.getText().toString());
                                            startActivity(intent);
                                            Toast.makeText(this, "Profile image stored to database successfully!", Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else {
                                            Toast.makeText(this, "An Error has occurred" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        });

                    }
                });
            } else{
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("");
                pDialog.setContentText("Error has ocurred while cropping image. Please try again.");
            }
        }

    }

    //TODO:"When more registration ways are created, change this method
    private void saveAccountInformation() {
        String username = mUsernameEditText.getText().toString();
        String firstName = mFirstNameEditText.getText().toString();
        String lastName = mLastNameEditText.getText().toString();
        String phoneNumber = mPhoneNumberEditText.getText().toString();

        if(TextUtils.isEmpty(username)){
            Toast.makeText(this, "No username entered!",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(firstName)){
            Toast.makeText(this, "No first name entered!",Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(lastName)){
            Toast.makeText(this, "No last name entered!",Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(phoneNumber)){
            Toast.makeText(this, "No phone number entered!",Toast.LENGTH_SHORT).show();
        } else{
            UserModel user = new UserModel();
            user.setUserId(mAuth.getCurrentUser().getUid());
            user.setUsername(username);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPhoneNumber(phoneNumber);
            user.setProfileImageUrl(downloadUrl);
            user.setEmail(Objects.requireNonNull(mAuth.getCurrentUser()).getEmail());
            user.setRegistrationDate(new Date());
            user.setAccountValidated(true);

            userRef.set(user)
                    .addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setContentText("Account Created Successfully!");
                    dialog.show();
                    dialog.setOnDismissListener(dialog1 -> {
                        goToMain();
                    });
                } else {
                    SweetAlertDialog dialog = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
                    String message = Objects.requireNonNull(task.getException()).getMessage();
                    dialog.setContentText(message);
                    dialog.show();
                   }
            });
        }
    }

    private void goToMain(){
        Intent intent = new Intent(SetupActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
