package com.d4u.drive4u.service;

import com.d4u.drive4u.domainModel.LocationModel;
import com.d4u.drive4u.dto.Location;

import java.util.List;

/**
 * Created by benjamin.sehic on 07.03.2018.
 */

public class LocationService {



    public LocationService(){
    }


    public static String convertToWaypointsQuerryParameters(List<Location> locationOLDList){

        if(locationOLDList == null || locationOLDList.size() == 0){
            return "";
        }
        StringBuilder waypoints = new StringBuilder();
        waypoints.append("optimize:true|");

            for(int i = 1; i < locationOLDList.size()-1; i++){
                waypoints.append(locationOLDList.get(i).toString());
                waypoints.append("|");
            }
            waypoints.append(locationOLDList.get(locationOLDList.size()-1).toString());

        return waypoints.toString();
    }

    public static LocationModel mapLocationToLocationModel(Location location){
        LocationModel locationModel = new LocationModel();
        locationModel.setLatitude(location.getLatitude());
        locationModel.setLongitude(location.getLongitude());
        locationModel.setTime(location.getTime());
        locationModel.setTitle(location.getTitle());
        return locationModel;
    }

    public static Location mapLocationModelToLocation(LocationModel locationModel){
        Location location = new Location();
        location.setLatitude(locationModel.getLatitude());
        location.setLongitude(locationModel.getLongitude());
        location.setTime(locationModel.getTime());
        location.setTitle(locationModel.getTitle());
        return location;
    }
}
