package com.d4u.drive4u.service;

import android.arch.lifecycle.MutableLiveData;

import com.d4u.drive4u.domainModel.UserModel;
import com.d4u.drive4u.dto.User;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by benjamin.sehic on 07.03.2018.
 */

public class UserService {

    private FirebaseFirestore db;

    public UserService(){
        db = FirebaseFirestore.getInstance();
    }

    public void addUser(User user){
        db.collection("users").add(user.toUserModel())
                .addOnSuccessListener(documentReference -> user.setUserId(documentReference.getId()))
                .addOnFailureListener(Throwable::printStackTrace);
    }

    public void addContact(SweetAlertDialog dialog,String loggedUserId, String otherUserId){
        db.collection("users").document(loggedUserId).get()
                .addOnSuccessListener(snapshot -> {
                    UserModel userModel = snapshot.toObject(UserModel.class);

                    if(userModel != null){
                        if(userModel.getContacts() == null){
                            userModel.setContacts(new ArrayList<>());
                        }
                        userModel.getContacts().add(otherUserId);
                        db.collection("users").document(loggedUserId).update("contacts",userModel.getContacts())
                                .addOnSuccessListener(aVoid -> {
                                    dialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                    dialog.setTitleText("Contact Added");
                                })
                                .addOnFailureListener(e -> {
                                    e.printStackTrace();
                                    dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                    dialog.setTitleText("Error");
                                    dialog.setContentTextSize(10);
                                    dialog.setContentText(e.getMessage());
                                });
                    }
                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    dialog.setTitleText("Error");
                    dialog.setContentTextSize(10);
                    dialog.setContentText(e.getMessage());
                });
    }

    public MutableLiveData<List<UserModel>> getContacts(List<String> contactIds){
        MutableLiveData<List<UserModel>> contactsLiveData = new MutableLiveData<>();
        List<UserModel> contacts = new ArrayList<>();

        for(String id : contactIds){
            db.collection("users").document(id).get()
                    .addOnSuccessListener(snapshot -> {
                        contacts.add(snapshot.toObject(UserModel.class));
                        contactsLiveData.setValue(contacts);
                    })
                    .addOnFailureListener(Throwable::printStackTrace);
        }

        return contactsLiveData;
    }



    public boolean containsContact(String id, List<String> contacts){
        for(String contactId : contacts){
            if(contactId.equals(id)){
                return true;
            }
        }
        return false;
    }

}
