package com.d4u.drive4u.fragment;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.d4u.drive4u.R;
import com.d4u.drive4u.adapter.ContactsAdapter;
import com.d4u.drive4u.domainModel.UserModel;
import com.d4u.drive4u.service.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ContactsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private RecyclerView mContactsRecyclerView;
    private RecyclerView.Adapter mContactsAdapter;
    private SweetAlertDialog pDialog;

    private LinearLayoutManager mContactsLayoutManager;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    private MutableLiveData<List<UserModel>> contactsLiveData;
    private UserService userService;

    public ContactsFragment() {
        userService = new UserService();
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }


    //region Fragment lifecycle methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_contacts, container, false);

        //region Testing
//        List<UserModel> userModels = new ArrayList<>();
//        UserModel userModel = new UserModel();
//        userModel.setUsername("mujomujic1");
//        userModel.setFirstName("Mujo");
//        userModel.setLastName("Mujic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("suljosuljic1");
//        userModel.setFirstName("Suljo");
//        userModel.setLastName("Suljic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("hasohasic1");
//        userModel.setFirstName("Haso");
//        userModel.setLastName("Hasic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("sabansabanovic1");
//        userModel.setFirstName("Saban");
//        userModel.setLastName("Sabanovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("hashimhashimovic1");
//        userModel.setFirstName("Hashim");
//        userModel.setLastName("Hashimovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("tahmetovic1");
//        userModel.setFirstName("Tarik");
//        userModel.setLastName("Ahmetovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("mujomujic1");
//        userModel.setFirstName("Mujo");
//        userModel.setLastName("Mujic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("suljosuljic1");
//        userModel.setFirstName("Suljo");
//        userModel.setLastName("Suljic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("hasohasic1");
//        userModel.setFirstName("Haso");
//        userModel.setLastName("Hasic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("sabansabanovic1");
//        userModel.setFirstName("Saban");
//        userModel.setLastName("Sabanovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("hashimhashimovic1");
//        userModel.setFirstName("Hashim");
//        userModel.setLastName("Hashimovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//        userModel = new UserModel();
//        userModel.setUsername("tahmetovic1");
//        userModel.setFirstName("Tarik");
//        userModel.setLastName("Ahmetovic");
//        userModel.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        userModels.add(userModel);
//endregion

        mContactsRecyclerView = v.findViewById(R.id.contacts_recycler_view);
        mContactsRecyclerView.setHasFixedSize(true);
        mContactsLayoutManager = new LinearLayoutManager(getContext());
        mContactsRecyclerView.setLayoutManager(mContactsLayoutManager);

        pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        DocumentReference loggedUserRef = db.collection("users").document(mAuth.getCurrentUser().getUid());

        loggedUserRef.get()
                .addOnSuccessListener(snapshot -> {
                    UserModel userModel = snapshot.toObject(UserModel.class);
                    if(userModel != null){
                        if(userModel.getContacts() != null){
                            contactsLiveData = userService.getContacts(userModel.getContacts());
                            contactsLiveData.observe(this,contacts ->{
                                if(mContactsAdapter == null){
                                    mContactsAdapter = new ContactsAdapter(getActivity(),contacts);
                                    mContactsRecyclerView.setAdapter(mContactsAdapter);
                                } else {
                                    mContactsAdapter.notifyDataSetChanged();
                                    mContactsRecyclerView.setAdapter(mContactsAdapter);
                                }
                                mContactsAdapter.notifyDataSetChanged();
                            });
                        }
                    }
                    pDialog.dismiss();
                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("");
                    pDialog.setContentTextSize(10);
                    pDialog.setContentText(e.getMessage());
                });


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //endregion
}
