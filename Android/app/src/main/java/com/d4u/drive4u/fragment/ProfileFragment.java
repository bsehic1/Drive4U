package com.d4u.drive4u.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.activity.MainActivity;
import com.d4u.drive4u.adapter.ProfileRoutesAdapter;
import com.d4u.drive4u.domainModel.UserModel;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.service.RouteService;
import com.d4u.drive4u.service.UserService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private ListView mRoutesListView;
    private ScrollView mScrollView;
    private CircleImageView mImageView;
    private TextView mFullNameTextView;
    private TextView mPhoneNumberTextView;
    private TextView mUsernameTextView;
    private RelativeLayout mDifferentUserRelativeLayout;
    private ImageButton mAddContactImageButton;
    private ImageButton mSendMessageImageButton;

    private int PERMISSION_CODE = 1;

    private List<Route> routeList;

    private ProfileRoutesAdapter profileRoutesAdapter;

    private FirebaseAuth mAuth;
    private DocumentReference userRef;
    private FirebaseFirestore db;

    private String userId;
    private DocumentReference loggedUserRef;
    private UserModel loggedUser;

    private UserService userService;

    public ProfileFragment() {
        db = FirebaseFirestore.getInstance();
        userService = new UserService();
    }

    //region Fragment lifecycle methods
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        mRoutesListView = v.findViewById(R.id.profile_routes_list_view);
        mScrollView = v.findViewById(R.id.profile_scroll_view);
        mImageView = v.findViewById(R.id.profile_image_view);
        mFullNameTextView = v.findViewById(R.id.profile_full_name_text_view);
        mPhoneNumberTextView = v.findViewById(R.id.profile_phone_number_text_view);
        mUsernameTextView = v.findViewById(R.id.profile_username_text_view);
        mDifferentUserRelativeLayout = v.findViewById(R.id.profile_different_user_relative_layout);
        mAddContactImageButton = v.findViewById(R.id.profile_add_contact_image_button);
        mSendMessageImageButton = v.findViewById(R.id.profile_send_message_image_button);

        SweetAlertDialog pDialog = new SweetAlertDialog(Objects.requireNonNull(this.getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(true);
        pDialog.show();

        mRoutesListView.setOnTouchListener((view, motionEvent) -> {
            mScrollView.requestDisallowInterceptTouchEvent(true);
            return false;
        });

        mRoutesListView.setOnItemClickListener((adapterView, view, i, l) -> {
            Route route = (Route) adapterView.getItemAtPosition(i);
            ((MainActivity) Objects.requireNonNull(getActivity())).goToRoutesDetail(route);
        });

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() != null){
            userRef = db.collection("users").document(userId);
            loggedUserRef = db.collection("users").document(mAuth.getCurrentUser().getUid());

            if(!userId.equals(mAuth.getCurrentUser().getUid())){
                mDifferentUserRelativeLayout.setVisibility(View.VISIBLE);
                loggedUserRef.get()
                        .addOnSuccessListener(snapshot -> {
                            loggedUser = snapshot.toObject(UserModel.class);
                            if(loggedUser != null){
                                if(loggedUser.getContacts() != null){
                                    if(!userService.containsContact(userId,loggedUser.getContacts())){
                                        mAddContactImageButton.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    mAddContactImageButton.setVisibility(View.VISIBLE);
                                }
                            }
                        })
                        .addOnFailureListener(Throwable::printStackTrace);
            }

            userRef.get()
                    .addOnSuccessListener(snapshot -> {
                        if(snapshot.contains("profileImageUrl")){
                            Picasso.get().load(snapshot.getString("profileImageUrl")).placeholder(R.drawable.profile_image_placeholder).into(mImageView);
                        }
                        if(snapshot.contains("username")){
                            mUsernameTextView.setText(snapshot.getString("username"));
                        }
                        if(snapshot.contains("firstName") && snapshot.contains("lastName")){
                            String fullName = snapshot.getString("firstName")+" "+snapshot.getString("lastName");
                            mFullNameTextView.setText(fullName);
                        }
                        if(snapshot.contains("phoneNumber")){
                            mPhoneNumberTextView.setText(snapshot.getString("phoneNumber"));
                        }
                        pDialog.dismissWithAnimation();
                    })
                    .addOnFailureListener(e -> {
                        e.printStackTrace();
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Error");
                        pDialog.setContentTextSize(10);
                        pDialog.setContentText(e.getMessage());
                    });
        }

        mAddContactImageButton.setOnClickListener(view->{
            SweetAlertDialog dialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            dialog.setTitleText("Loading");
            dialog.setCancelable(true);
            dialog.show();
            userService.addContact(dialog, Objects.requireNonNull(mAuth.getCurrentUser()).getUid(),userId);
            mAddContactImageButton.setVisibility(View.GONE);
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //endregion

    private void getRoutesByUserId() {

        RouteService routeService = new RouteService();
        routeService.getRoutesByUserId(Objects.requireNonNull(mAuth.getCurrentUser()).getUid()).observe(this, routes -> {
            routeList = routes;
            if(profileRoutesAdapter == null){
                profileRoutesAdapter = new ProfileRoutesAdapter(Objects.requireNonNull(getActivity()), routeList);
                mRoutesListView.setAdapter(profileRoutesAdapter);
            } else{
                profileRoutesAdapter.notifyDataSetChanged();
            }
        });
    }

    //region Permission Check Methods
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", (dialog, which) -> ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                            new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permissions GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permissions DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //endregion

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
