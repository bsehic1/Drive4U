package com.d4u.drive4u.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {

    private EditText mUserEmailEditText;
    private EditText mUserPasswordEditText;
    private EditText mUserConfirmPasswordEditText;
    private Button mNextButton;
    private SweetAlertDialog pDialog;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        mUserEmailEditText = findViewById(R.id.register_email_edit_text);
        mUserPasswordEditText = findViewById(R.id.register_password_edit_text);
        mUserConfirmPasswordEditText = findViewById(R.id.register_confirm_password_edit_text);
        mNextButton = findViewById(R.id.register_next_step_button);


        mNextButton.setOnClickListener(view -> {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(false);
            pDialog.show();

            CreateNewAccount();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser != null){
            goToMain();
        }
    }

    private void CreateNewAccount() {
        String email = mUserEmailEditText.getText().toString();
        String password = mUserPasswordEditText.getText().toString();
        String confirmPassword = mUserConfirmPasswordEditText.getText().toString();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Email is empty!",Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Password is empty!",Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(confirmPassword)){
            Toast.makeText(this,"Confirm password is empty!",Toast.LENGTH_SHORT).show();
        } else if(!TextUtils.equals(password,confirmPassword)){
            Toast.makeText(this,"Password does not match!",Toast.LENGTH_SHORT).show();
        } else {

            mAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            goToSetup();
                            Toast.makeText(this, "Authenticated", Toast.LENGTH_SHORT).show();
                        } else{
                            String message = Objects.requireNonNull(task.getException()).getMessage();
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("");
                            pDialog.setContentText(message);
                        }
                    });
        }
    }



    private void goToMain(){
        Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void goToSetup(){
        Intent intent = new Intent(RegisterActivity.this, SetupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
