package com.d4u.drive4u;

import android.app.Application;

import com.d4u.drive4u.dal.D4URealmConfiguration;

import io.realm.Realm;

/**
 * Created by benjamin.sehic on 26.02.2018.
 */

public class Drive4UApplication extends Application {
    @Override
    public void onCreate(){
        super.onCreate();
        Realm.init(this);
        Realm.setDefaultConfiguration(D4URealmConfiguration.getRealmConfiguration());
    }
}
