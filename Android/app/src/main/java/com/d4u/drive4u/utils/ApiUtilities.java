package com.d4u.drive4u.utils;

import com.d4u.drive4u.repository.DirectionsRepository;

/**
 * Created by benjamin.sehic on 02.03.2018.
 */

public class ApiUtilities {

    private static final String DIRECTIONS_API_BASE_URL = "https://maps.googleapis.com/maps/api/directions/";

    public static DirectionsRepository getDirectionRepository(){
        return RetrofitClient.getClient(DIRECTIONS_API_BASE_URL).create(DirectionsRepository.class);
    }
}
