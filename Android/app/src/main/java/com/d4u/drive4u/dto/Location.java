package com.d4u.drive4u.dto;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Location {

    private Double longitude;

    private Double latitude;

    private String title;

    private Date time;

    public Location(){

    }

    public Location(LatLng latLng){
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }

    //region Getters and Setters
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    //endregion

    @Override
    public String toString() {
        return latitude.toString() + "," + longitude.toString();
    }

    public void setLatLon(LatLng latLng){
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }
}
