package com.d4u.drive4u.adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.d4u.drive4u.R;
import com.d4u.drive4u.dto.Location;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

public class CreateWaypointsAdapter extends ArrayAdapter<Location> {
    private Context _context;
    private List<Location> locationList;

    public CreateWaypointsAdapter(@NonNull Context context, List<Location> locations){
        super(context, 0, locations);
        _context = context;
        locationList = locations;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(_context).inflate(R.layout.create_waypoint_list_item,parent,false);

        EditText mTitleEditText = listItem.findViewById(R.id.waypoint_list_item_title_text_view);
        EditText mTimeEditText = listItem.findViewById(R.id.waypoint_list_item_time_text_view);
        TextView mNumberEditText = listItem.findViewById(R.id.waypoint_list_item_number_text_view);

        mNumberEditText.setText(String.valueOf(position+1));

        String title = locationList.get(position).getTitle();
        if(title != null && !title.isEmpty()){
            mTitleEditText.setText(title);
        } else {
            locationList.get(position).setTitle(mTitleEditText.getText().toString());
        }

        String time;

        if(locationList.get(position) != null && locationList.get(position).getTime() != null){
            time = DateFormat.getTimeInstance(DateFormat.SHORT).format(locationList.get(position).getTime());
            mTimeEditText.setText(time);
        }

        mTimeEditText.setOnClickListener(v -> {
            Calendar mCalendar = Calendar.getInstance();
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getContext(), (view, hourOfDay, minute) -> {
                mCalendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                mCalendar.set(Calendar.MINUTE,minute);
                locationList.get(position).setTime(mCalendar.getTime());
                String newTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(mCalendar.getTime());
                mTimeEditText.setText(newTime);
            },0,0,true);
            mTimePicker.setTitle("Set Waypoint Time");
            mTimePicker.show();
        });

        return listItem;
    }

    @Override
    public int getCount() {
        if (locationList == null) {
            return 0;
        } else {
            return locationList.size();
        }
    }
}