package com.d4u.drive4u.service;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.d4u.drive4u.utils.ApiUtilities;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.repository.DirectionsRepository;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by benjamin.sehic on 07.03.2018.
 */

public class RouteService {

    private FirebaseFirestore db;

    public RouteService(){
        db = FirebaseFirestore.getInstance();
    }

    public void addRoute(Route route){

        db.collection("routes").add(route)
                .addOnSuccessListener(snapshot-> route.setRouteId(snapshot.getId()))
                .addOnFailureListener(Throwable::printStackTrace);
    }

    public MutableLiveData<List<Route>> getRoutesByUserId(String id){
        MutableLiveData<List<Route>> routesLiveData = new MutableLiveData<>();
        ArrayList<Route> routes = new ArrayList<>();
        db.collection("routes").whereEqualTo("createdById",id).get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<DocumentSnapshot> routeSnapshots = queryDocumentSnapshots.getDocuments();
                    for(DocumentSnapshot documentSnapshot : routeSnapshots){
                        routes.add(documentSnapshot.toObject(Route.class));
                    }
                    routesLiveData.setValue(routes);
                })
                .addOnFailureListener(Throwable::printStackTrace);

        return routesLiveData;
    }

    public MutableLiveData<Route> getRouteByRouteId(String id){
        MutableLiveData<Route> routeLiveData = new MutableLiveData<>();
        db.collection("routes").document(id).get()
                .addOnSuccessListener(snapshot -> {
                    Route route = snapshot.toObject(Route.class);
                    if(route != null) {
                        route.setRouteId(snapshot.getId());
                        routeLiveData.setValue(route);
                        Log.d("Route retrieved: ", snapshot.getId());
                    }
                })
                .addOnFailureListener(Throwable::printStackTrace);
        return routeLiveData;
    }

    public MutableLiveData<List<String>> getDirectionsData(Location origin, Location destination, @Nullable List<Location> throughLocationsList){

        MutableLiveData<List<String>> directionsLiveData = new MutableLiveData<>();

        DirectionsRepository directionsRepository = ApiUtilities.getDirectionRepository();
        String waypointsQuerryParameters = LocationService.convertToWaypointsQuerryParameters(throughLocationsList);
        Single<String> directionsSingle = directionsRepository.getDirections(origin.toString(),destination.toString(),waypointsQuerryParameters, "AIzaSyBg_4j-R3YiNLxiPtqXylpeNNEbslNSGEw");
        directionsSingle.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("Subscribed", d.toString());
                    }

                    @Override
                    public void onSuccess(String stringResponse) {
                        directionsLiveData.setValue(getDirectionsData(stringResponse));
                    }

                    @Override
                    public void onError(Throwable e){
                        e.printStackTrace();
                    }
                });
        return directionsLiveData;
    }

    private ArrayList<String> getDirectionsData(String response){
        JSONObject obj;
        ArrayList<String> wp = new ArrayList<>();
        try {
            obj = new JSONObject(response);
            JSONArray routes = obj.getJSONArray("routes");
            ArrayList<JSONArray> legList = new ArrayList<>();
            ArrayList<JSONArray> stepList = new ArrayList<>();
            for(int i = 0; i < routes.length(); i++){
                legList.add(routes.getJSONObject(i).getJSONArray("legs"));
            }
            for(JSONArray jsonArray : legList){
                for(int i = 0; i < jsonArray.length(); i++){
                    stepList.add(jsonArray.getJSONObject(i).getJSONArray("steps"));
                }
            }
            for(JSONArray jsonArray : stepList){
                for(int i = 0; i < jsonArray.length(); i++){
                    wp.add(jsonArray.getJSONObject(i).getJSONObject("polyline").get("points").toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wp;
    }
}
