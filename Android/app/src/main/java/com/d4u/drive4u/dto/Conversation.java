package com.d4u.drive4u.dto;

import java.util.List;

public class Conversation {

    private String conversationId;

    private List<String> participants;

    private List<Message> messages;

    private Conversation(){

    }

}
