package com.d4u.drive4u.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.fragment.ContactsFragment;
import com.d4u.drive4u.fragment.CreateRouteDetailsFragment;
import com.d4u.drive4u.fragment.ProfileFragment;
import com.d4u.drive4u.fragment.RouteFragment;
import com.d4u.drive4u.fragment.CreateRouteOriginAndDestinationFragment;
import com.d4u.drive4u.fragment.CreateRouteWaypointsFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements
        ProfileFragment.OnFragmentInteractionListener,
        ContactsFragment.OnFragmentInteractionListener,
        RouteFragment.OnFragmentInteractionListener,
        CreateRouteDetailsFragment.OnFragmentInteractionListener{

    private DrawerLayout mDrawerLayout;

    private FirebaseAuth mAuth;
    DocumentReference loggedUserRef;

    public static String UID = "nHFEOWTZ5EUcqN4yC1Ga";
    public static String otherUID = "WxiBglA7bRxGV44dXur7";
    private int PERMISSION_CODE = 1;

    //region Activity Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocalBroadcastManager.getInstance(this).registerReceiver(mHandler,new IntentFilter("com.d4u.drive4u_FCM-MESSAGE"));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        ImageButton mSearchButton = findViewById(R.id.search_button);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View mNavigationHeader = navigationView.inflateHeaderView(R.layout.navigation_header);

        CircleImageView mNavigationHeaderProfileImageView = mNavigationHeader.findViewById(R.id.navigation_header_profile_image_view);
        TextView mNavigationHeaderProfileTextView = mNavigationHeader.findViewById(R.id.navigation_header_profile_text_view);

        mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser() == null){
            goToLogin();
            finish();
        }

        loggedUserRef = FirebaseFirestore.getInstance().collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());

        loggedUserRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    String profile_image_url = documentSnapshot.getString("profileImageUrl");
                    String username = documentSnapshot.getString("username");
                    if(profile_image_url != null && !profile_image_url.isEmpty()){
                        Picasso.get().load(profile_image_url).placeholder(R.drawable.profile_image_placeholder).into(mNavigationHeaderProfileImageView);
                    }
                    if(username != null && !username.isEmpty()){
                        mNavigationHeaderProfileTextView.setText(username);
                    }
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this,"Couldn't load profile information!",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                });

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions();
        }

        navigateDrawerLayout(navigationView);

        mSearchButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, SearchableActivity.class);
            startActivity(intent);
        });

    //region testing
//        //TESTING:
//        List<UserModel> firestoreUsers = new ArrayList<>();
//        UserModel firestoreUser = new UserModel();
//        firestoreUser.setUsername("mujomujic1");
//        firestoreUser.setFirstName("Mujo");
//        firestoreUser.setLastName("Mujic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("suljosuljic1");
//        firestoreUser.setFirstName("Suljo");
//        firestoreUser.setLastName("Suljic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("hasohasic1");
//        firestoreUser.setFirstName("Haso");
//        firestoreUser.setLastName("Hasic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("sabansabanovic1");
//        firestoreUser.setFirstName("Saban");
//        firestoreUser.setLastName("Sabanovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("hashimhashimovic1");
//        firestoreUser.setFirstName("Hashim");
//        firestoreUser.setLastName("Hashimovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("tahmetovic1");
//        firestoreUser.setFirstName("Tarik");
//        firestoreUser.setLastName("Ahmetovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("mujomujic1");
//        firestoreUser.setFirstName("Mujo");
//        firestoreUser.setLastName("Mujic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("suljosuljic1");
//        firestoreUser.setFirstName("Suljo");
//        firestoreUser.setLastName("Suljic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("hasohasic1");
//        firestoreUser.setFirstName("Haso");
//        firestoreUser.setLastName("Hasic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("sabansabanovic1");
//        firestoreUser.setFirstName("Saban");
//        firestoreUser.setLastName("Sabanovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("hashimhashimovic1");
//        firestoreUser.setFirstName("Hashim");
//        firestoreUser.setLastName("Hashimovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//        firestoreUser = new UserModel();
//        firestoreUser.setUsername("tahmetovic1");
//        firestoreUser.setFirstName("Tarik");
//        firestoreUser.setLastName("Ahmetovic");
//        firestoreUser.setProfileImageUrl("https://firebasestorage.googleapis.com/v0/b/drive4u-1522069295472.appspot.com/o/Profile%20Images%2FDcwYQrhkSuSjgt0eycjWwIyToEX2.jpg?alt=media&token=1c960a41-fec5-4539-a1ee-becae898de58");
//        firestoreUsers.add(firestoreUser);
//
//
//        UserModel haso = new UserModel();
//        haso.setFirstName("FIRSTNAME");
//        haso.setLastName("LASTNAME");
//        haso.setEmail("EMAIL");
//        haso.setUsername("USERNAME");
//        haso.setPhoneNumber("PHONE");
//        haso.setAccountValidated(true);
//        haso.setRegistrationDate(new Date());
//
//

//        CollectionReference usersCollection = FirebaseFirestore.getInstance().collection("users");
//
//        usersCollection.add(haso).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//            @Override
//            public void onSuccess(DocumentReference documentReference) {
//                Toast.makeText(MainActivity.this, "HASO JE KREIRAN", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        //OTHER TESTING
//        RouteService routeService = new RouteService();
//        Route route = new Route();
//        route.setCreatedById("DcwYQrhkSuSjgt0eycjWwIyToEX2");
//        route.setCreatedDate(new Date());
//        route.setDescription("This is the first route I'm creating on Firebase!");
//        route.setRouteDate(new Date());
//        route.setTitle("My First Route On Firebase");
//
//        Location origin = new Location();
//        origin.setTitle("origin");
//        origin.setTime(new Date());
//        origin.setLongitude(3d);
//        origin.setLatitude(4d);
//
//        Location destination = new Location();
//        destination.setTitle("destination");
//        destination.setTime(new Date());
//        destination.setLongitude(3d);
//        destination.setLatitude(4d);
//
//        Location stanica1 = new Location();
//        stanica1.setTitle("stanica 1");
//        stanica1.setTime(new Date());
//        stanica1.setLongitude(3d);
//        stanica1.setLatitude(4d);
//
//        Location stanica2 = new Location();
//        stanica2.setTitle("stanica 2");
//        stanica2.setTime(new Date());
//        stanica2.setLongitude(3d);
//        stanica2.setLatitude(4d);
//
//        Location stanica3 = new Location();
//        stanica3.setTitle("stanica 3");
//        stanica3.setTime(new Date());
//        stanica3.setLongitude(3d);
//        stanica3.setLatitude(4d);
//
//        Location stanica4 = new Location();
//        stanica4.setTitle("stanica 4");
//        stanica4.setTime(new Date());
//        stanica4.setLongitude(3d);
//        stanica4.setLatitude(4d);
//
//        route.setOrigin(origin);
//        route.setDestination(destination);
//        ArrayList<Location> haso = new ArrayList<>();
//        haso.add(stanica1);
//        haso.add(stanica2);
//        haso.add(stanica3);
//        haso.add(stanica4);
//        route.setLocations(haso);
//
//        routeService.addRoute(route);
//enregion

    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null){
            goToLogin();
        } else{
            checkUserExistence();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent incomingIntent = getIntent();
        if(incomingIntent.getExtras() != null){
            if(incomingIntent.getExtras().containsKey("profile")){
                goToProfileFragment(incomingIntent.getStringExtra("profile"));
            }
        }
    }

    //endregion

    //region Permission Check Methods
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            new AlertDialog.Builder(getApplicationContext())
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", (dialog, which) -> ActivityCompat.requestPermissions(MainActivity.this,
                            new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permissions GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permissions DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //endregion

    //region Drawer Menu methods
    private void navigateDrawerLayout(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(item -> {
            item.setChecked(true);
            mDrawerLayout.closeDrawers();

            // Handle item selection
            switch (item.getItemId()) {
                case R.id.nav_profile:
                    ProfileFragment profileFragment = new ProfileFragment();
                    profileFragment.setUserId(mAuth.getCurrentUser().getUid());
                    changeFragment(profileFragment);
                    return true;
                case R.id.nav_contacts:
                    changeFragment(new ContactsFragment());
                    return true;
                case R.id.nav_routes:
                    changeFragment(new CreateRouteOriginAndDestinationFragment());
                    return true;
                case R.id.nav_logut:
                    mAuth.signOut();
                    goToLogin();
                default:
                    return super.onOptionsItemSelected(item);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region Activity and Fragment navigation Methods
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void changeFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public void goToRoutesDetail(Route route){
        RouteFragment routeFragment = new RouteFragment();
        routeFragment.setRoute(route);
        changeFragment(routeFragment);
    }

    public void goToProfileFragment(String userId){
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setUserId(userId);
        changeFragment(profileFragment);
    }

    public void goToWaypointsFragment(Route route, @Nullable List<Location> locationList){
        CreateRouteWaypointsFragment createRouteWaypointsFragment = new CreateRouteWaypointsFragment();
        createRouteWaypointsFragment.setRoute(route);
        if(locationList != null){
            createRouteWaypointsFragment.setLocationList(locationList);
        }
        changeFragment(createRouteWaypointsFragment);
    }

    public void goToOriginAndDestinationFragment(@Nullable Route route){
        CreateRouteOriginAndDestinationFragment createRouteOriginAndDestinationFragment = new CreateRouteOriginAndDestinationFragment();
        if(route != null){
            createRouteOriginAndDestinationFragment.setRoute(route);
        }
        changeFragment(createRouteOriginAndDestinationFragment);
    }

    public void goToCreateRouteDetailsFragment(Route route, List<Location> locationList){
        CreateRouteDetailsFragment createRouteDetailsFragment = new CreateRouteDetailsFragment();
        createRouteDetailsFragment.setRoute(route);
        createRouteDetailsFragment.setLocationList(locationList);
        changeFragment(createRouteDetailsFragment);
    }

    private void goToLogin(){
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void goToSetup(){
        Intent intent = new Intent(MainActivity.this,SetupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void goToContactFragment(){
        changeFragment(new ContactsFragment());
    }
    //endregion

    //TODO:"Potrebno je promijeniti ovu metodu na adekvatan dizajn"
    private void checkUserExistence() {
        String currentUserId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        DocumentReference userRef = FirebaseFirestore.getInstance().collection("users").document(currentUserId);

        userRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    if(documentSnapshot.getString("username") == null || Objects.requireNonNull(documentSnapshot.getString("username")).isEmpty() ||
                            documentSnapshot.getString("firstName") == null || Objects.requireNonNull(documentSnapshot.getString("firstName")).isEmpty() ||
                            documentSnapshot.getString("lastName") == null || Objects.requireNonNull(documentSnapshot.getString("lastName")).isEmpty() ||
                            documentSnapshot.getString("phoneNumber") == null || Objects.requireNonNull(documentSnapshot.getString("phoneNumber")).isEmpty() ||
                            documentSnapshot.getString("profileImageUrl") == null || Objects.requireNonNull(documentSnapshot.getString("profileImageUrl")).isEmpty()){
                        goToSetup();
                    }
                })
                .addOnFailureListener(e -> goToSetup());
    }

    private BroadcastReceiver mHandler = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String message = intent.getStringExtra("message");

            Toast.makeText(MainActivity.this, title, Toast.LENGTH_SHORT).show();
            Toast.makeText(MainActivity.this, message,Toast.LENGTH_LONG).show();
        }
    };
}
