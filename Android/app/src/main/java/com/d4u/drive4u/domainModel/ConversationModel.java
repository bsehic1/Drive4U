package com.d4u.drive4u.domainModel;

import java.util.List;

public class ConversationModel {

    private String conversationId;

    private List<String> participants;

    public ConversationModel(){

    }

    //region Getters and Setters
    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }
    //endregion
}
