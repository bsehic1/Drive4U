package com.d4u.drive4u.dto;

import java.util.Date;

import javax.annotation.Nullable;

public class Message {

    private String messageId;

    private String conversationId;

    private String senderId;

    private String messageText;

    private Date sentTimeStamp;

    @Nullable
    private Date receivedTimestamp;

    public Message(){

    }
}
