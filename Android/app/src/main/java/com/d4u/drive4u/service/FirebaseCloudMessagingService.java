package com.d4u.drive4u.service;

import android.util.Log;

import com.d4u.drive4u.domainModel.TextMessageModel;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.support.constraint.Constraints.TAG;
import static com.d4u.drive4u.activity.MainActivity.UID;

public class FirebaseCloudMessagingService {

    private FirebaseFirestore db;

    public FirebaseCloudMessagingService(){
        db = FirebaseFirestore.getInstance();
    }

    public void startConversationWith(List<String> participantIdList){
        Map<String,Object> newConversation = new HashMap<>();

        newConversation.put("participants",participantIdList);

        db.collection("conversations")
                .add(newConversation)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    Map<String,Object> firstMessage = new HashMap<>();
                    firstMessage.put("sent_timestamp",new Date());
                    firstMessage.put("sender_id", db.collection("users").document(UID).getId());
                    firstMessage.put("message_text", "Started a conversation");

                    documentReference.collection("messages").add(firstMessage);
                })
                .addOnFailureListener(exception -> {
                    Log.w(TAG, "Error adding document", exception);
                });
    }

    public void sendMessage(TextMessageModel textMessageModel){
        Map<String,Object> newMessage = new HashMap<>();

        newMessage.put("sender_id", textMessageModel.getSenderId());
        newMessage.put("message_text", textMessageModel.getMessageText());
        newMessage.put("sent_timestamp", textMessageModel.getSentTimeStamp());
        newMessage.put("received_timestamp", textMessageModel.getReceivedTimestamp());

        db.collection("conversations/" + textMessageModel.getConversationId() + "/messages")
                .add(newMessage)
                .addOnSuccessListener(documentReference -> {
                    Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                })
                .addOnFailureListener(exception -> {
                    Log.w(TAG, "Error adding document", exception);
                });
    }

    public void getTextMessagesByConversationId(String conversationId){
        DocumentReference documentReference = db.collection("conversations").document(conversationId);

        documentReference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                } else {
                    Log.d(TAG, "No such document");
                }
            } else {
                Log.d(TAG, "get failed with ", task.getException());
            }
        });
    }



}
