package com.d4u.drive4u.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.d4u.drive4u.R;
import com.d4u.drive4u.dto.Location;
import com.d4u.drive4u.dto.Route;
import com.d4u.drive4u.service.RouteService;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RouteFragment extends Fragment implements OnMapReadyCallback {

    private MapView mMapView;
    private GoogleMap mGoogleMap;
    private ListView mLocationsListView;
    private SweetAlertDialog pDialog;

    private RouteService routeService;

    private List<Marker> markerList;

    private int PERMISSION_CODE = 1;

    private Route route;

    public RouteFragment() {
        markerList = new ArrayList<>();
        routeService = new RouteService();
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_route, container, false);

        pDialog = new SweetAlertDialog(Objects.requireNonNull(getActivity()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        mLocationsListView = v.findViewById(R.id.route_locations_list_view);
        ScrollView routeScrollView = v.findViewById(R.id.route_scroll_view);

        mMapView = v.findViewById(R.id.route_map_map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        mLocationsListView.setOnTouchListener((view, motionEvent) -> {
            routeScrollView.requestDisallowInterceptTouchEvent(true);
            return false;
        });

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

        drawPolylineByLocations(googleMap,route.getLocations());

        for(Location location : route.getLocations()){
            LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
            Marker marker = googleMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(location.getTitle()));
            markerList.add(marker);
        }

        if(ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions();
        }

        googleMap.setMyLocationEnabled(true);

        pDialog.dismissWithAnimation();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.857498, 18.421586), 12);
        googleMap.animateCamera(cameraUpdate);
    }

    private void drawPolylineByLocations(GoogleMap googleMap, List<Location> locationList) {
        if(locationList.size() > 1){
            routeService.getDirectionsData(route.getOrigin(),route.getDestination(),route.getLocations()).observe(this, waypoints->{
                if (waypoints != null) {
                    for (String line : waypoints) {
                        PolylineOptions polylineOptions = new PolylineOptions()
                                .color(Color.BLUE)
                                .width(15)
                                .addAll(PolyUtil.decode(line));
                        googleMap.addPolyline(polylineOptions);
                    }
                }
            });
        }
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    //region Map Fragment lifecycle methods
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
    //endregion

    //region Permission Check Methods
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(Objects.requireNonNull(getActivity()),
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", (dialog, which) -> ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                            new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE))
                    .setNegativeButton("cancel", (dialog, which) -> dialog.dismiss())
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permissions GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permissions DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //endregion

}
